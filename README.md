# My git template

The main purpose of this template is to implement a feature that I think
should exist in git out of the box: the ability to add hooks without having
to edit code.

Traditionally, this is done by editing the hook script and making it call
whatever scripts you'd like. That's not good enough.

The way this template works is by providing a `recursive-hook.sample` script
that should be callable as any hook, which will then look for executable scripts
in a directory name `$hook-name.d` and call the scripts in order.

## Usage

    git clone https://gitlab.com/jjatria/git-template/ ~/.git-template
    git config --global init.templatedir '~/.git-template/template'

Calling `git init` after this will copy the contents of the `template` dir in this
repo into your `.git` directory together with the rest of the `git` skeleton. This
will work even on existing repos.

By default, _this changes nothing in your repos_. To "enable" it, you'll have to
do the following

    cd $THEREPO
    git init
    cd .git/hooks
    mv recursive-hook.sample recursive-hook
    chmod 755 recursive-hook

    # Move any hooks you already have into the appropriate directory, eg
    mv post-checkout post-checkout.d/00-foo

    # Enable the recursive-hook for that particular one
    ln -s recursive-hook post-checkout

Profit!

## Acknowledgements

[Tim Pope's 'Effortless Ctags with Git' post][post] was the main inspiration for this.

## TODO

Some hooks use STDIN / STDOUT. These are not currently forwarded correctly to the
nested scripts.

[post]: https://tbaggery.com/2011/08/08/effortless-ctags-with-git.html
